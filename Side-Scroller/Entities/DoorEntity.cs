﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Side_Scroller.Entities
{
    class DoorEntity : Entity
    {
        public DoorEntity()
        {
            Width = 32;
            Heigth = 64;
        }

        public override bool Blocks(Entity entity)
        {
            if (entity is PlayerEntity)
                return false;

            return true;
        }

        public override void Draw(SpriteBatch sb, Texture2D spriteSheet)
        {
            base.Draw(sb, spriteSheet);

            var drawX = (int)(X - Level.CamX);
            var drawY = (int)(Y - Level.CamY);

            sb.Draw(spriteSheet, new Rectangle(drawX, drawY, (int)Width, (int)Heigth), new Rectangle(32, 32, 32, 64), Color.White);
        }
    }
}
