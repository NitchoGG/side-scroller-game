﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Side_Scroller.Entities
{
    class PlayerEntity : Entity
    {
        private const float Friction = 0.299f;

        private int screenWidth;
        private int screenHeigth;
        private float deltaY;
        private float deltaX;
        public PlayerEntity()
        {
            Width = 32;
            Heigth = 64;

        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var keyboard = Keyboard.GetState();
            deltaX *= Friction;

            if (!IsGrounded)
            {
                deltaY += Level.Gravity;
            }
            else
            {
                deltaY = 0;
            }

            Console.WriteLine("Grounded: " + IsGrounded);

            if (keyboard.IsKeyDown(Keys.Up) && IsGrounded)
            {
                deltaY -= Level.Gravity * 10;
            }
            if (keyboard.IsKeyDown(Keys.Left))
            {
                deltaX--;
            }
            if (keyboard.IsKeyDown(Keys.Right))
            {
                deltaX++;
            }

            X += deltaX;
            Y += deltaY;

            Level.CamX = X - screenWidth / 2 + Heigth / 2;
        }


        public override void Draw(SpriteBatch sb, Texture2D spriteSheet)
        {
            base.Draw(sb, spriteSheet);

            var drawX = (int)(X - Level.CamX);
            var drawY = (int)(Y - Level.CamY);

            sb.Draw(spriteSheet, new Rectangle(drawX,drawY , (int)Width, (int)Heigth), new Rectangle(0, 32, 32, 64), Color.White);

            screenWidth = sb.GraphicsDevice.Viewport.Bounds.Width;
            screenHeigth = sb.GraphicsDevice.Viewport.Bounds.Height;
        }


    }
}
