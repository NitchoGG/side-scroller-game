﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Side_Scroller.Physics;
using Side_Scroller.Tiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Side_Scroller.Entities
{
    class Entity : AABB
    {
        public bool Active { get; set; }
        public Level Level { get; set; }

        public Entity()
        {
            Active = true;
            Owner = this;
        }

        public virtual void Draw(SpriteBatch sb, Texture2D spriteSheet)
        {

        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual bool Blocks(Entity entity)
        {
            return true;
        }
    }
}
