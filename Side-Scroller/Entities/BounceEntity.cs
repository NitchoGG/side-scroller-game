﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Side_Scroller.Entities
{
    class BounceEntity : Entity
    {
        public BounceEntity()
        {
            Width = 32;
            Heigth = 32;

        }

        public override void Draw(SpriteBatch sb, Texture2D spriteSheet)
        {
            base.Draw(sb, spriteSheet);

            var drawX = (int)(X - Level.CamX);
            var drawY = (int)(Y - Level.CamY);

            sb.Draw(spriteSheet, new Rectangle(drawX, drawY, (int)Width, (int)Heigth), new Rectangle(194, 0, 32, 32), Color.White);
        }

        public override bool Blocks(Entity entity)
        {
            if (entity is PlayerEntity)
                return false;

            return true;
        }
    }
}
