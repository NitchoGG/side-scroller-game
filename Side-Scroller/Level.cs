﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Side_Scroller.Entities;
using Side_Scroller.Physics;
using Side_Scroller.Tiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Side_Scroller
{
    class Level
    {
        public float CamX { get; set; }
        public float CamY { get; set; }
        public float Gravity { get; set; }

        private int[,] tileIds;
        private List<Entity> entities;
        private List<Entity> newEntities;
        private List<AABB> collisions;

        public Level()
        {
            collisions = new List<AABB>();
            entities = new List<Entity>();
            newEntities = new List<Entity>();

            tileIds = new int[,]
            {
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,4,4,4,3},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,4,4,4,3},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,3},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,3},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
            };

            AddEntity(new PlayerEntity() { X = 0, Y = 0});
            AddEntity(new DoorEntity(){ X = 18*32, Y = 5*32});
            AddEntity(new BounceEntity() { X = 4*32, Y = 9*32 });

            Gravity = 2;
        } 

        public void AddEntity(Entity entity)
        {
            entity.Level = this;
            newEntities.Add(entity);
        }

        public void Update(GameTime gameTime)
        {
            entities.AddRange(newEntities);
            newEntities.Clear();

            for(int i = 0; i < entities.Count; i++)
            {
                var entity = entities[i];

                if(entity.Active)
                {
                    entity.Update(gameTime);
                    entity.IsGrounded = false;
                }
                else
                {
                    entities.RemoveAt(i);
                    i--;
                }
            }

            collisions.Clear();
            collisions.AddRange(entities);

            for (int i = 0; i < tileIds.GetLength(0); i++)
            {
                for (int j = 0; j < tileIds.GetLength(1); j++)
                {
                    var tile = Tile.GetTile(tileIds[i, j]);

                    if(!(tile is AirTile))
                    {
                        collisions.Add(tile.GetAABB(j * Tile.tileSize, i * Tile.tileSize));
                    }
                }
            }

            foreach (var aabb1 in collisions)
            {
                foreach (var aabb2 in collisions)
                {
                    var blocks = true;

                    if (aabb2.Owner is Tile && aabb1.Owner is Entity)
                    {
                        var entity = aabb1 as Entity;
                        var tile = aabb2.Owner as Tile;
                        blocks = tile.IsBlocks(entity);
                    }

                    var doorBlocks = true;

                    if(aabb2.Owner is DoorEntity && aabb1.Owner is PlayerEntity)
                    {
                        
                        var player = aabb1 as PlayerEntity;
                        var door = aabb2 as DoorEntity;

                        doorBlocks = door.Blocks(player);
                    }

                    //var bounceblocks = true;

                    //if (aabb2.owner is bounceentity && aabb1.owner is playerentity)
                    //{

                    //    var player = aabb1 as playerentity;
                    //    var bounce = aabb2 as bounceentity;

                    //    bounceblocks = bounce.blocks(player);
                    //}

                    if (aabb1 != aabb2 && blocks && doorBlocks)
                    {
                        var r1 = new Rectangle((int)aabb1.X, (int)aabb1.Y, (int)aabb1.Width, (int)aabb1.Heigth);
                        var r2 = new Rectangle((int)aabb2.X, (int)aabb2.Y, (int)aabb2.Width, (int)aabb2.Heigth);
                        var overlap = Rectangle.Intersect(r1, r2);

                        if (!overlap.IsEmpty)
                        {
                            var d1x = aabb1.X + aabb1.Width - overlap.X;
                            var d2x = overlap.X + overlap.Width + aabb1.X;
                            var dx = Math.Abs(d1x) < Math.Abs(d2x) ? d1x : d2x;

                            var d1y = aabb1.Y + aabb1.Heigth - overlap.Y;
                            var d2y = -(overlap.Y + overlap.Height - aabb1.Y);
                            var dy = Math.Abs(d1y) < Math.Abs(d2y) ? d1y : d2y;

                            if (Math.Abs(dx) < Math.Abs(dy))
                            {
                                aabb1.IsGrounded = false;
                                aabb1.X -= dx;
                            }
                            else
                            {
                                aabb1.IsGrounded = true;
                                aabb1.Y -= dy;
                            }
                        }
                    }
                }
            }
        }

        public void Draw(SpriteBatch sb, Texture2D spriteSheet)
        {
            CamY = ((tileIds.GetLength(1) - 11) * Tile.tileSize) - sb.GraphicsDevice.Viewport.Bounds.Height;

            for (int i = 0; i < tileIds.GetLength(0); i++)
            {
                for (int j = 0; j < tileIds.GetLength(1);j++)
                { 
                    var id = tileIds[i, j];

                    var tile = Tile.GetTile(id);

                    tile.Draw(sb, spriteSheet, j * Tile.tileSize - (int)CamX, i * Tile.tileSize-(int)CamY);
                }
            }

            foreach(var entity in entities)
            {
                entity.Draw(sb, spriteSheet);
            }

            var rnd = new Random();

            foreach (var col in collisions)
            {
                var r = (float)rnd.NextDouble();
                var g = (float)rnd.NextDouble();
                var b = (float)rnd.NextDouble();

                sb.Draw(spriteSheet, new Rectangle((int)(col.X - CamX), (int)(col.Y - CamY), (int)col.Width, (int)col.Heigth), new Rectangle(64, 0, 32, 32), new Color(r, g, b));
            }
        }

        public Tile GetTile(float x, float y)
        {
            if(x < 0 || x >= tileIds.GetLength(0) || y < 0 || y >= tileIds.GetLength(1))
            {
                return Tile.AirTile;
            }

            var tile = Tile.GetTile(tileIds[(int)x, (int)y]);

            return tile;
        }
    }


}
