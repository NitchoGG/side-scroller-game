﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Side_Scroller.Physics
{
    class AABB
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Width { get; set; }
        public float Heigth { get; set; }
        public bool IsGrounded { get; set; }
        public object Owner { get; set; }
    }
}
