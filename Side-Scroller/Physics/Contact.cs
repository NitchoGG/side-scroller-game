﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Side_Scroller.Physics
{
    class Contact
    {
        internal float Distance { get; set; }
        internal float NormalX { get; set; }
        internal float NormalY { get; set; }
        internal bool Intersects { get; set; }
    }
}
