﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Side_Scroller.Tiles
{
    class GrassTile : Tile
    {
        public GrassTile(int id) : base(id)
        {
        }

        public override void Draw(SpriteBatch sb, Texture2D spriteSheet, int x, int y)
        {
            base.Draw(sb,spriteSheet,x,y);

            sb.Draw(spriteSheet, new Rectangle(x, y, tileSize, tileSize),new Rectangle(0,0,32,32), Color.White);
        }
    }
}
