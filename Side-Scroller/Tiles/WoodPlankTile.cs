﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Side_Scroller.Entities;

namespace Side_Scroller.Tiles
{
    class WoodPlankTile : Tile
    {
        public WoodPlankTile(int id) : base(id)
        {
        }

        public override bool IsBlocks(Entity entity)
        {
            return false;
        }

        public override void Draw(SpriteBatch sb, Texture2D spriteSheet, int x, int y)
        {
            base.Draw(sb, spriteSheet, x, y);

            sb.Draw(spriteSheet, new Rectangle(x, y, tileSize, tileSize), new Rectangle(161, 0, 32, 32), Color.White);
        }
    }
}
