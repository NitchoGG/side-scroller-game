﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Side_Scroller.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Side_Scroller.Tiles
{
    class WoodNoBlockTile : Tile
    {
        public WoodNoBlockTile(int id) : base(id)
        {
        }

        public override void Draw(SpriteBatch sb, Texture2D spriteSheet, int x, int y)
        {
            base.Draw(sb, spriteSheet, x, y);

            sb.Draw(spriteSheet, new Rectangle(x, y, tileSize, tileSize), new Rectangle(96, 0, 32, 32), Color.White);
        }

        public override bool IsBlocks(Entity entity)
        {
            return false;
        }

    }
}
