﻿using Microsoft.Xna.Framework.Graphics;
using Side_Scroller.Entities;
using Side_Scroller.Physics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Side_Scroller.Tiles
{
    class Tile
    {
        private static Tile[] tiles = new Tile[6];
        public static readonly AirTile AirTile = new AirTile(0);
        public static readonly GrassTile GrassTile = new GrassTile(1);
        public static readonly StoneTile StoneTile = new StoneTile(2);
        public static readonly WoodTile WoodTile = new WoodTile(3);
        public static readonly WoodPlankTile WoodPlankTile = new WoodPlankTile(4);
        public static readonly WoodNoBlockTile WoodNoBlockTile = new WoodNoBlockTile(5);

        public const int tileSize = 32;

        public int ID { get; private set; }

        protected Tile(int id)
        {
            this.ID = id;
            tiles[id] = this;
        }

        public virtual void Draw(SpriteBatch sb, Texture2D spriteSheet, int x, int y)
        {

        }

        public virtual bool IsBlocks(Entity entity)
        {
            return true;
        }

        public static Tile GetTile(int id)
        {
            return tiles[id];
        }

        public AABB GetAABB(float x, float y)
        {
            return new AABB()
            {
                X = x,
                Y = y,
                Width = tileSize,
                Heigth = tileSize,
                Owner = this
            };
        }
    }
}
