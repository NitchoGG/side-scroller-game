﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Side_Scroller.Entities;

namespace Side_Scroller.Tiles
{
    class AirTile : Tile
    {
        public AirTile(int id) : base(id)
        {

        }

        public override bool IsBlocks(Entity entity)
        {
            return false;
        }
    }
}
